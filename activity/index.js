const students = ['Adrian', 'Aaron', 'Alexis', 'Blue', 'Nikko', 'Masahiro', 'Bianca', 'Carlo'];


function addStudent(name) {
	students.push(name);
	console.log(name + ' was added to the students\' list')
	console.log(students)
}


function countStudents() {
	console.log("The total number of students are: " + students.length);
}


function printStudents() {
	console.log("Student Names in order:")
	students.sort();
	students.forEach(function(name){
		console.log(name);
	})
}

addStudent('Angelo');
countStudents();
printStudents();
